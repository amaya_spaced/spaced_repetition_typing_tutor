#!/usr/bin/python3
#WARN:THIS PROGRAM MAY OVERWRITE ALREADY EXISTS FILES
unique_suffix='Y4CuVF52o9Y'
#algorithm's configurable parameters
INF=float('inf')
MINIMAL_DURATION=1.
MAX_FREEZE_COUNT=999999
MAX_FREEZE_TIME=INF
MAX_ANSWER_WAIT_TIME=1.
MAX_RECOGNITION_WAIT_TIME=2.
basic_keys=frozenset("qwertyuiopasdfghjklzxcvbnm")
#time_data[0] is review_time_point, time_data[1] is duration
time_data=[]
keys=[]
from curses import use_default_colors,wrapper,flushinp
from time import time
from pickle import load,dump
from os import replace
import readline
from bisect import bisect_left
import sys
layout_name=sys.argv[1]
keys_info_file_path='{}.{}'.format(layout_name,unique_suffix)
temporary_file_path='{}.tmp.{}'.format(layout_name,unique_suffix)
def save(mode='wb'):
    while True:
        try:
            f=open(temporary_file_path,mode)
            dump((keys,time_data),f)
            f.close()
            replace(temporary_file_path,keys_info_file_path)
        except KeyboardInterrupt:
            continue
        else:
            break
try:
    f=open(keys_info_file_path,'rb')
except FileNotFoundError:
    keys=sorted(basic_keys)
    time_data=[(time()+MINIMAL_DURATION,MINIMAL_DURATION)]*len(keys)
else:
    keys,time_data=load(f)
    f.close()
def register():
    Local_KeyboardInterrupt=KeyboardInterrupt
    local_keys=keys
    local_keys_insert=keys.insert
    local_time_data=time_data
    local_time_data_insert=time_data.insert
    local_bisect_left=bisect_left
    local_time=time
    local_input=input
    local_len=len
    LOCAL_MINIMAL_DURATION=MINIMAL_DURATION
    while True:
        for string in local_input('register:\n').strip().lower().split():
            try:
                index=bisect_left(local_keys,string)
                if local_len(local_keys)!=index and local_keys[index]==string:
                    local_time_data[index]=(local_time()+LOCAL_MINIMAL_DURATION,LOCAL_MINIMAL_DURATION)
                elif string.isalpha():
                    local_keys_insert(index,string)
                    local_time_data_insert(index,(local_time()+LOCAL_MINIMAL_DURATION,LOCAL_MINIMAL_DURATION))
            except Local_KeyboardInterrupt:
                #data corrupted
                quit()
                
def delete():
    Local_KeyboardInterrupt=KeyboardInterrupt
    local_basic_keys=basic_keys
    local_keys=keys
    local_time_data=time_data
    local_bisect_left=bisect_left
    local_input=input
    local_len=len
    while True:
        for string in local_input('delete:\n').strip().lower().split():
            try:
                if string not in local_basic_keys:
                    index=local_bisect_left(local_keys,string)
                    if local_len(local_keys)!=index and local_keys[index]==string:
                        del local_keys[index]
                        del local_time_data[index]
            except Local_KeyboardInterrupt:
                #data corrupted
                quit()
def review(win):
    Local_KeyboardInterrupt=KeyboardInterrupt
    LOCAL_MINIMAL_DURATION=MINIMAL_DURATION
    LOCAL_MAX_FREEZE_COUNT=MAX_FREEZE_COUNT
    LOCAL_MAX_FREEZE_TIME=MAX_FREEZE_TIME
    LOCAL_MAX_ANSWER_WAIT_TIME=MAX_ANSWER_WAIT_TIME
    LOCAL_MAX_RECOGNITION_WAIT_TIME=MAX_RECOGNITION_WAIT_TIME
    LOCAL_INF=INF
    local_len=len
    local_enumerate=enumerate
    local_getkey=win.getkey
    local_time=time
    local_keys=keys
    local_time_data=time_data
    flushinp()
    use_default_colors()
    count=0
    current_errors=0
    current_freeze_count=LOCAL_MAX_FREEZE_COUNT
    while True:
        detected_key=''
        timeouted=0
        current_errors=0
        current_freeze_count+=1
        if  current_freeze_count>LOCAL_MAX_FREEZE_COUNT or current_freeze_count>local_len(keys) or local_time()-current_time>LOCAL_MAX_FREEZE_TIME:
            current_freeze_count=1
            current_time=local_time()
        #loop to get best index to review
        remembered_best_index=None
        remembered_best_proportion=-LOCAL_INF
        forgotten_best_index=None
        forgotten_best_proportion=LOCAL_INF
        for index,(review_time_point,duration) in local_enumerate(local_time_data):
            current_proportion=(current_time-review_time_point)/duration
            #remembered
            if current_time<review_time_point:
                if current_proportion>remembered_best_proportion:
                    remembered_best_proportion=current_proportion
                    remembered_best_index=index
            else:
                if current_proportion<forgotten_best_proportion:
                    forgotten_best_proportion=current_proportion
                    forgotten_best_index=index
        current_index=remembered_best_index if forgotten_best_index is None else forgotten_best_index
        current_key=local_keys[current_index]
        review_time_point,duration=local_time_data[current_index]
        while True:
            user_inputed_string=''
            for c in current_key:
                win.clear()
                win.addstr("""'{}'
'{}'
'{}'
timeouted:{}
current_errors:{}
count:{}
current_freeze_count:{}
proportion:{}
time_length:{}
elapsed:{}
total_keys:{}""".format(
                        current_key,
                        user_inputed_string,
                        detected_key,
                        timeouted,
                        current_errors,
                        count,
                        current_freeze_count,
                        (current_time-review_time_point)/duration,
                        duration,
                        current_time-(review_time_point-duration),
                        local_len(local_keys)))
                answer_start_time=local_time()
                detected_key=local_getkey()
                answer_end_time=local_time()
                if answer_end_time-answer_start_time>(LOCAL_MAX_ANSWER_WAIT_TIME if user_inputed_string else LOCAL_MAX_RECOGNITION_WAIT_TIME):
                    real_current_time=local_time()
                    duration=real_current_time-(review_time_point-duration)
                    if duration<LOCAL_MINIMAL_DURATION:
                        duration=LOCAL_MINIMAL_DURATION
                    review_time_point=real_current_time+duration
                    while True:
                        try:
                            local_time_data[current_index]=(review_time_point,duration)
                        except Local_KeyboardInterrupt:
                            continue
                        else:
                            break
                    timeouted+=1
                    if detected_key!=c:
                        current_errors+=1
                        break
                elif detected_key!=c:
                    real_current_time=local_time()
                    duration=real_current_time-(review_time_point-duration)
                    if duration<LOCAL_MINIMAL_DURATION:
                        duration=LOCAL_MINIMAL_DURATION
                    review_time_point=real_current_time+duration
                    while True:
                        try:
                            local_time_data[current_index]=(review_time_point,duration)
                        except Local_KeyboardInterrupt:
                            continue
                        else:
                            break
                    current_errors+=1
                    break
                user_inputed_string+=detected_key
            else:
                real_current_time=local_time()
                duration+=real_current_time-(review_time_point-duration)
                if duration<LOCAL_MINIMAL_DURATION:
                        duration=LOCAL_MINIMAL_DURATION
                review_time_point=real_current_time+duration
                while True:
                        try:
                            local_time_data[current_index]=(review_time_point,duration)
                        except Local_KeyboardInterrupt:
                            continue
                        else:
                            break
                count+=1
                break
try:
    while True:
        user_input=input('({})input function:\n'.format(layout_name))
        try:
            if user_input=='r':
                wrapper(review)
            elif user_input=='a':
                register()
            elif user_input=='s':
                save()
                print('saved')
            elif user_input=='d':
                delete()
        except (EOFError,KeyboardInterrupt):
            pass
except (EOFError,KeyboardInterrupt):
            save()
            print('saved and quit')
